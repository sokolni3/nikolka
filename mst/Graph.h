//
// Created by sokol on 29. 1. 2020.
//

#ifndef SEM_GRAPH_H
#define SEM_GRAPH_H

#include <iostream>
#include <vector>
#include <algorithm>
#include "Edge.h"

using namespace std;
/** Struct represents disjoin sets */
class DisjointSet {
public:
    int parent = 0;
    int rnk = 0;

    DisjointSet() = default;

    /** This method finds set for vertex u. */
    int find_set(int &u);

    /** This method unions sets -u, -v based on their rank. */
    void union_set(int &u, int &v);
};

class Graph {
public:
    // V-> Number of vertices, E-> Number of edges
    int V = 0;
    int E = 0;
    int current_index_e = 0; // current index of an edge
    int current_index_v = 0; // current index of a vertex
    Edge *edges; // List of edges in graph G
    int *vertices; // List of vertices
    DisjointSet *Ds; // Disjoint set for vertices in G

    // Graph constructor
    Graph(int V, int E) {
        this->V = V;
        this->E = E;
        this->edges = new Edge[E];
        this->vertices = new int[V];
        // for each v ∈ G.V do: make-set(v)
        this->Ds = new DisjointSet[(this->V)];
        this->makeSets();
    }

    /** This method adds edge to a graph G. */
    void add_edge(int &src, int &dest, int weight);

    /** This method adds vertex to a graph G. */
    void add_vertex(int &v);

    /** This method computes minimum spanning tree of a graph. */
    void mst_computation();

    /** Function MakeSet(x) is:
        if x is not already present then
        add x to the disjoint-set tree
        x.parent := x
        x.rank   := 0
        x.size   := 1 */
    void makeSets();
};

#endif //SEM_GRAPH_H
