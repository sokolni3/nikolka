//
// Created by sokol on 29. 1. 2020.
//

#ifndef SEM_Edge_H
#define SEM_Edge_H

using namespace std;

class Edge {
public:
    int weight = 0; // Weight of vertex
    int src = 0; // Source vertex
    int dest = 0; // Destination vertex

    /** Method sets edge properties. */
    void setEdgeProperties(int &src, int &dest, int weight) {
        this->src = src;
        this->dest = dest;
        this->weight = weight;
    }

    /** Comparison operator < */
    bool operator<(Edge const&e) const;
};


#endif //SEM_Edge_H
