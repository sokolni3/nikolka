//
// Created by sokol on 31. 1. 2020.
//

#include "Graph.h"

void Graph::add_edge(int &src, int &dest, int weight) {
    // If edge is already in a graph G, do not add this edge again.
    if(this->current_index_e > 0) {
        int e = current_index_e;
        for (int i = 0; i < e; i++) {
            if (edges[i].dest == src && edges[i].src == dest && edges[i].weight == weight) {
                return;
            }
        }
    }

    // Else add edge to a G
    edges[current_index_e].setEdgeProperties(src, dest, weight);
    current_index_e++;
}

void Graph::add_vertex(int &v) {
    vertices[current_index_v] = v;
    current_index_v++;
}

void Graph::makeSets() {
    for (int v = 0; v < V; ++v) {
        Ds[v].parent = v;
        Ds[v].rnk = 0;
    }
}

/**
 * function Find(x) is
    root := x
    while root.parent ≠ root
        root := root.parent

    while x.parent ≠ root
        parent := x.parent
        x.parent := root
        x := parent

  return root
 * @param u int
 * @return set of a vertex u
 */
int DisjointSet::find_set(int &u) {
    // Parent of itself return i
    if (this[u].parent == u) return u;
    // Else find set of vertex v
    return find_set(this[u].parent);
}

/**
 *  function Union(x, y) is
   xRoot := Find(x)
   yRoot := Find(y)

   // x and y are already in the same set
   if xRoot = yRoot then
       return

   // x and y are not in same set, so we merge them
   if xRoot.rank < yRoot.rank then
     xRoot, yRoot := yRoot, xRoot // swap xRoot and yRoot

   // merge yRoot into xRoot
   yRoot.parent := xRoot
   if xRoot.rank = yRoot.rank then
     xRoot.rank := xRoot.rank + 1
 * @param u
 * @param v
 */
void DisjointSet::union_set(int &u, int &v) {
    int u_set = this->find_set(u);
    int v_set = this->find_set(v);
    if(this[u_set].rnk == this[v_set].rnk) {
        this[v_set].parent = u_set;
        this[u_set].rnk++;
        return;
    } else if(this[u_set].rnk > this[v_set].rnk) {
        this[v_set].parent = u_set;
        return;
    }
    this[u_set].parent = v_set;
}

/** This method prints minimum spanning tree of a graph G. */
void printMST(pair<int, Edge *> const &pair) {
    cout << "Minimum spanning tree of graph G is: \n";
    int weight_mst = 0;
    for (int i = 0; i < pair.first; ++i) {
        weight_mst += pair.second[i].weight;
        cout << "Edge: " << pair.second[i].src << " - " << pair.second[i].dest << " with weight: " << pair.second[i].weight << "\n";
    }
    cout << "Weight of mst is: " << weight_mst << "\n";
}

/** This method saves computet minimum spanning tree of graph G to a file. */
void visualiseMST(Graph const &graph) {

}

/** Kruskal´s algorithm:
    A := ∅
    for each v ∈ G.V do
        MAKE-SET(v)
    for each (u, v) in G.E ordered by weight(u, v), increasing do
        if FIND-SET(u) ≠ FIND-SET(v) then
           A := A ∪ {(u, v)}
           UNION(FIND-SET(u), FIND-SET(v))
    return A

 * @return pair: int - number of edges in computed mst and array of Edges.
 * */
void Graph::mst_computation(){
    cout << "Start mst computation: \n";
    // A := ∅
    // for each (u, v) in G.E ordered by weight(u, v), increasing do
    // order edges by its weight
    if(this != nullptr) {
        vector<Edge> edges; // V-1 edges
        const int v_index = current_index_v;
        const int e_index = current_index_e;
        Edge mst[v_index];
         // int - source vertex, int - destination vertex, int - weight
         for (int i = 0; i < e_index; i++) {
             edges.push_back(this->edges[i]);
         }

         sort(edges.begin(), edges.end());

         int current_index = 0;
         // mst have exactly V-1 edges
         while (current_index < v_index - 1) {
             for (auto &e : edges) {
                 // Find indexes of vertices
                 int index_u, index_v = 0;
                 for (int i = 0; i < v_index; i++) {
                     if (this->vertices[i] == e.src) {
                         index_u = i;
                     }
                     if (this->vertices[i] == e.dest) {
                         index_v = i;
                     }
                 }

                 int u = Ds->find_set(index_u);
                 int v = Ds->find_set(index_v);
                 // if FIND-SET(u) ≠ FIND-SET(v) then
                 if (u != v) {
                     // A := A ∪ {(u, v)}
                     mst[current_index++] = e;

                     // UNION(FIND-SET(u), FIND-SET(v))
                     Ds->union_set(u, v);
                 }
             }
         }

         printMST({current_index, mst});
    } else {
        cout << endl << "Graph is nullptr and can not be computed.\n";
    }
}
