#include <iostream>
#include <bits/stdc++.h>
#include <thread>
#include "Graph.h"


using namespace std;

mutex graph_mutex;

template <typename TimePoint>
std::chrono::microseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::microseconds>(tp);
}

namespace timer {
    auto now() {
        return std::chrono::high_resolution_clock::now();
    }
}

/**
 * Method loads graph from a file.
 * Format of a text file:
 * <počet-vrcholů> <počet-hran>
   0 <počet hran z vrcholu 0> <cílový vrchol 1> <délka 1. hrany> <cílový vrchol 2> <délka 2. hrany> ...
   1 <počet hran z vrcholu 1> <cílový vrchol 1> <délka 1. hrany> <cílový vrchol 2> <délka 2. hrany> ...
 * return Graph
 * */
void loadGraphFromFile(string const &path) {
    if(path.size() > 1) {
        cout << "Start loading graph from file " << path << "\n";
        std::ifstream ifs(path);

        if (ifs.is_open()) {

            // Initialize graph G
            int V, E = 0;

            // read first line from ifstream ifs
            ifs >> V >> E;
            if (V != 0 && E != 0) {
                // create a new graph
            Graph *g = new Graph(V, E);

                // For every vertex in graph g read line from ifstream
                for (int i = 0; i < V; i++) {
                    // First string is a source vertex
                    string vertex = "";
                    ifs >> vertex;
                    int v = stoi(vertex);
                    g->add_vertex(v);

                    // Second string is number of edges from vertex
                    string number_of_edges = "";
                    ifs >> number_of_edges;
                    int edges = stoi(number_of_edges);

                    // And they are followed by destination vertices and weights of edges
                    for (int j = 0; j < edges; j++) {
                        // destination vertex
                        string destination = "";
                        ifs >> destination;
                        int u = stoi(destination);

                        // weight of edge
                        string weight = "";
                        ifs >> weight;
                        int w = stoi(weight);
                        g->add_edge(v, u, w);
                    }
                }


                cout << "Graph was succesfully loaded.\n";

                g->mst_computation();
            } else {
                return;
            }
        }
    } else {
        return;
    }
}

/** This is method for initialization filePaths to files which contain graph description.
 * You can choose from: graph1.txt, graph2.txt, graph3.txt, graph.txt.
 * */
vector<string> getFilePaths() {
    vector<string> filePaths = {"graph1.txt","graph2.txt", "graph3.txt", "graph.txt"};
    return filePaths;
}

void run_single_thread(){
    // Choose from files: graph1.txt, graph2.txt, graph3.txt, graph4.txt
    loadGraphFromFile("graph4.txt");
}

void run_multi_thread(string const &path) {
    std::lock_guard<std::mutex> guard(graph_mutex);
    if(!path.empty()) {
        loadGraphFromFile(path);
    } else {
        cout << endl << "Minimum spanning tree can not be computed because graph is nullptr.\n";
    }

}

void run_multi_thread_computation() {
    vector<thread> threads = {}; // List of threads in a graph
    vector<string> filePaths = getFilePaths(); // List of file paths

    // Set threads list size and threads
    int j = filePaths.size();
    for(unsigned int i = 0; i < j; ++i) {
        threads.emplace_back(run_multi_thread, ref(filePaths.at(i)));
    }

    // Join threads
    for(int i = 0; i < j; ++i) {
        threads[i].join();
    }
}

void compare() {
    cout << "\nComparation of single and multi -thread implementation: \n";
    // RUN single-thread implementation
    auto from_ST = timer::now();
    run_single_thread();
    auto to_ST = timer::now();
    auto ST = to_ms(to_ST-from_ST).count();

    // RUN multi-thread implementation
    auto from_MT = timer::now();
    run_multi_thread_computation();
    auto to_MT = timer::now();
    auto MT = to_ms(to_MT-from_MT).count();

    cout << "\nST time neeeded(microseconds): " << ST << ".\n";
    cout << "MT time neeeded(microseconds): " << MT << ".\n";
    if(ST < MT) {
        cout << "\nFaster is implementation of single thread computation.\n";
    } else {
        cout << "\nFaster is implementation of multi thread computation.\n";
    }
}


void help() {
    cout << "This is semestral project for PJC created by Nikola Sokolova and represents an implementation of Kruskal´s algorithm." << "\n" <<
         "Please use following command-line arguments: " << "\n" <<
         "-single_thread   Calculates mst of graph from graph.txt and prints results." << "\n" <<
         "-multi_thread   Calculates msts of graphs from vector of different graph.txt files and prints results." << "\n" <<
         "-compare   Compares single and multi -thread computation." << "\n";
}

int main(int argc, char **argv) {

    if (argc != 2) {
        cout << "Cannot run mst computation. Wrong number of command-line arguments. Please use -help.\n";
        return 1;
    }

    auto argument = std::string(argv[1]);

    if (argument == "-help") {
        help();
        return 0;

    } else if (argument == "-single_thread") {
        run_single_thread();
        return 0;

    } else if (argument == "-multi_thread") {
        run_multi_thread_computation();
        return 0;
    } else if (argument == "-compare") {
        compare();
        return 0;

    } else {
        cout << "Cannot run mst computation. Please use -help.";
    }

}
