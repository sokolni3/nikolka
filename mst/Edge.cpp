//
// Created by sokol on 31. 1. 2020.
//

#include <tuple>
#include "Edge.h"

using namespace std;

bool Edge::operator<(Edge const &e) const {
    return tie(weight, src, dest) < tie(e.weight, e.src, e.dest);
}
